#Realiza o download e instalação do pacote php5-fpm
class php5 {
  package { 'php5-fpm':
    ensure  => installed, # para garantir que o pacote esteja disponivel e atualizado
    require => Class['system-update'],
  }

  #Garante que o serviço do php estará rodando após a instalação do pacote
  service { 'php5-fpm':
    ensure  => 'running',
    require => Package['php5-fpm'],
  }
}
