class ssh {

  file {'/usr/share/nginx/www/ssh/':
    ensure  => directory,
    require => Class['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {'/usr/share/nginx/www/ssh/index.html':
    ensure  => file,
    require => Package['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/ssh/index.html',
  }
}
