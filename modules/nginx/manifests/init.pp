class nginx {
  package { 'nginx':
    ensure  => installed,
    require => [Class['system-update'], File['/etc/nginx/nginx.conf']],
  }

  file {'/etc/nginx/':
    ensure => directory,
    notify => Service['nginx'],
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  file {'/etc/nginx/nginx.conf':
    ensure => directory,
    notify => Service['nginx'],
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/nginx/nginx.conf',
  }

  service { 'nginx':
      ensure  => 'running',
      require => Package['nginx'],
  }
}
