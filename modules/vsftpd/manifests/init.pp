class vsftpd {
  # Instala o pacote nginx e vsftpd após garantir que a lista de pacotes está atualizada
  package { [ 'vsftpd' ]:
    ensure  => 'installed',
    require => Class['system-update'],
  }

  file {'/usr/share/nginx/www/vsftpd/':
    ensure  => directory,
    require => Class['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {'/usr/share/nginx/www/vsftpd/index.html':
      ensure  => file,
      require => Class['nginx'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/vsftpd/index.html',
  }

  # Copia os arquivos de configuração do servidor FTP & Reinicia-o
  exec {'copia_config':
      command => 'sudo cp -rf /vagrant/vsftpd.conf /etc/vsftpd.conf; sudo restart vsftpd',
      onlyif  => ['ls -l /etc/vsftpd.conf'],
      require => Package['vsftpd']
  }
}
