class dovecot {

  #Realiza a instalação do dovecot, se certificando de que o postfix já foi instalado
  package { ['dovecot-common', 'dovecot-imapd']:
    ensure  => installed,
    require => Class['postfix']
  }

  #Inicializa o serviço do dovecot imap
  service { 'dovecot':
    ensure  => 'running',
    require => Package['dovecot-imapd']
  }

  #Copia os arquivos de configuração do dovecot para as respectivas pastas
  file { 'dovecot.conf':
    ensure  => file,
    path    => '/etc/dovecot/dovecot.conf',
    require => Package['dovecot-imapd'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/dovecot/dovecot.conf';
  }

  file { '10-auth.conf':
    ensure  => file,
    path    => '/etc/dovecot/conf.d/10-auth.conf',
    require => File['dovecot.conf'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/dovecot/conf.d/10-auth.conf';
  }

  file { '10-mail.conf':
    ensure  => file,
    notify  => Service['dovecot'],
    path    => '/etc/dovecot/conf.d/10-mail.conf',
    require => File['10-auth.conf'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/dovecot/conf.d/10-mail.conf';
  }

  file { '20-imap.conf':
    ensure  => file,
    notify  => Service['dovecot'],
    path    => '/etc/dovecot/conf.d/20-imap.conf',
    require => File['20-imap.conf'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/dovecot/conf.d/20-imap.conf';
  }
}
